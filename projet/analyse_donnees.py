import os 
import shutil
import numpy as np
import json
from class_photoset import photoset
from os import listdir ,mkdir
from os.path import isdir,isfile,join
from scipy.stats import pearsonr
from calcul_lever_coucher import concentration
import matplotlib.pyplot as plt


def Correlation_point(data):
    """
    
    Renvoie la correlation suivant l'algoritme de pearson
    entre les valeur de brightness et de daylight 
    Args:
        (class photoset): Element de la classe photoset
    Returns:
        (float): coefficient de correlation     
    """
    brightness = data.get_brightness()
    daylight = data.get_daylight()
    return pearsonr(brightness, daylight)[0]

def Create_dir(dir_name):
    """
    Creer le repertoire dir_name si inexistant sinon le supprime 
    et le recrée

    Args:
        dir_name(str): nom du repertoire à creer

    """
    if not isdir(dir_name):
        mkdir(dir_name)
    else:
        shutil.rmtree(dir_name)
        mkdir(dir_name)

def Classement(dir, dir_dest, seuil):

    """
    Classe les fichiers du répertoire dir dans le répertoire de destination dir_dest selon trois sous dossiers :
        non_corr (non corrélés)
        non_lever_coucher (pas de valeurs de lever ET de coucher)
        non_outdoor (trop de photos prises indoor)

    Args:
        dir(str): PATH du répertoire à trier
        dir_dest(str): PATH du répertoire où se trouveront les fichiers triés
        seuil(float): Seuil de correlation de daylight et brightness
    Output:
        Arborescence repertoire comme suit
            -> dir_dest
                -> Analysable (Fichier Analysable)
                -> non_corr (Donnée respectant tout les condition précédente excepté la correlation)
                -> non_lever_coucher(Fichier n'etant pas indoor mais ne donnant le lever et le coucher du soleil)
                -> non_outdoor (Fichier n'etant pas outdoor )
    """

    fichiers = [join(dir, f)
                    for f in os.listdir(dir)
                    if isfile(join(dir, f))
                    ]                                       # Ensemble de fichier no no_gps
    # Construire le repertoire de destination
    # Creer les repertoires de destination
    dir_dest_analysable = join(dir, "analysable")
    Create_dir(dir_dest_analysable)

    # De meme pour les correlation inferieur
    dir_dest_non_outdoor = join(dir_dest, "non_outdoor")
    dir_dest_non_corr = join(dir_dest, "non_corr")
    dir_dest_non_lever_coucher = join(dir_dest, "non_lever_coucher")
    Create_dir(dir_dest_non_outdoor)
    Create_dir(dir_dest_non_corr)
    Create_dir(dir_dest_non_lever_coucher)

    for filepath in fichiers:                        # Pour tout les fichier de du
        # Recuperer les donnée du fichier
        data = photoset(filepath)
        # Calculer la correlation
        cor = Correlation_point(data)
        # de la brightness et de daylight
        if (sum([i < 0.2 for i in data.get_outdoor()]) < 70):
            if nb_partitions_daylight_basse(data) == 2:
                if cor > seuil:
                    # Copier le fichier si la correlation
                    shutil.copy(filepath, dir_dest_analysable)
                    # superieur au seuil
                else:
                    shutil.copy(filepath, dir_dest_non_corr)
            else:
                shutil.copy(filepath, dir_dest_non_lever_coucher)
        else:
            shutil.copy(filepath, dir_dest_non_outdoor)


def duree_photoset(data):
    """
    Renvoie la duree du photoset data au format datetime_json

    Args:
        data(class photoset): un objet de la classe photoset
        
    Returns:
        float : datetime_json    
    """
    ps_sorted = sorted(data.photos, key =lambda d: d["datetime"])
    return ps_sorted[-1]["datetime"] - ps_sorted[0]["datetime"]


def Classement_duree_du_photoset(doss):
    '''
    Classe les photoset contenus dans les fichiers du dossier doss 
    selon leur dureee de prise de vue Retourne une liste des 
    photosets tries avec leur duree
    Args:
        doss(str): PATH du dossier à trier
    Returns:
        (str,int): liste triée selon la duree de tuple du nom du fichier et de la duree du fichier
    Ex : 
        [('85d79a8c.json', 0.8032966844380098), ('23ed4a97.json', 0.9332101965867423)]
    '''
     
    photo_sets_tries = []
    for f in os.listdir(doss):
        data = photoset(join(doss,f))
        ps_duree = duree_photoset(data)
        photo_sets_tries.append((data.nom_fichier,ps_duree))
    
    photo_sets_tries = sorted(photo_sets_tries, key = lambda d: d[1])
    
    return photo_sets_tries 


def photosets_partitiones_selon_duree(doss):
    """Affiche une partition des photosets calssés selon leur durée de prise de vue

    Args:
        doss (str): PATH du répertoire à trier

    Returns:
        dict: dictionnaire avec pour chaque durée (clé du dico), une liste des fichiers dans cette interval
        Exemple :
        Fichiers du dossier "analysables gps" triés selon leur duree:
{'Moins 1 semaine': ['94fe5580.json', '6d6b2948.json', 'ea212b58.json', 'ae68d894.json', '35b6eb60.json', 'b3e1a3e9.json', 'c6b9f2fc.json', '2e302584.json', '941ea3db.json'], 'Entre 1 semaine et 1 mois': ['2984e0b2.json', '8d883fde.json', '065e1d53.json', '19ec674a.json', '7811e2fd.json'], 'Entre 1 mois et 3 mois': ['a4ed29bc.json', '0784f2f0.json', 'f624aefa.json', 'f0c76939.json', '8c6f0d8a.json'], 'Entre 3 mois et 6 mois': ['716a851d.json', 'fd7c3e78.json', '0a16502f.json'], 'Plus de 6 mois': ['c8094bb1.json', 'e5cb5e82.json', '84b5bb60.json', '69a0b8d1.json', '1ab73e07.json', '85d79a8c.json', '0f3e7eb6.json', '1c7e31db.json', 'a9da00e7.json']}
        """

    dico_part = {"Moins 1 semaine":[], "Entre 1 semaine et 1 mois":[], "Entre 1 mois et 3 mois":[], "Entre 3 mois et 6 mois":[], "Plus de 6 mois":[]}
    for ps, d in Classement_duree_du_photoset(doss):
        duree_ps = duree_photoset(photoset(join(doss, ps)))
        if duree_ps < 7/365:
            # moins de 1 semaine
            dico_part["Moins 1 semaine"].append(ps)
        elif duree_ps < 31/365:
            # entre 1 semaine et 1 mois
            dico_part["Entre 1 semaine et 1 mois"].append(ps)
        elif duree_ps < 91/365:
            # entre 1 mois et 3 mois
            dico_part["Entre 1 mois et 3 mois"].append(ps)
        elif duree_ps < 0.5:
            # entre 3 mois et 6 mois
            dico_part["Entre 3 mois et 6 mois"].append(ps)
        else:
            # plus de 6 mois
            dico_part["Plus de 6 mois"].append(ps)

    return dico_part


def affichage_partition(data, est_partition=False, tod_lever=0.25, tod_coucher=0.75, midi_solaire=0.5, donnees_calculees=False, analysable=False, non_correlation=False, non_outdoor=False):
    """
    Affiche les donnees de brightness, daylight en fonction du time_of_day 
    et midi solaire, lever et coucher d'un photoset si donnees_calculees=True
    sur un graphe
    Args:
        data(class photoset) : un objet de class photoset, si est_partition est True, alors c'est une liste
        est_partition(bool) : à true si l'objet a afficher est une partition (liste) et non un objet photoset
        tod_lever(float): lever du soleil time_of_day
        tod_coucher(float): coucher du soleil time_of_day
        midi_solaire(float): midi solaire time_of_day
        donnée calculée(bool): affiche les donnees de midi solaire, lever et coucher si a True
        analysable(bool) : Si data est analysable
        non_correlaton(bool) : Si data est  non correlée
        non_outdoor(bool) : Si la plupaat des photos de data est indoor 
    

    """
    if est_partition:
        time_of_day = [ sub['time_of_day'] for sub in data ]
        daylight = [ sub['daylight'] for sub in data ]
        brightness = [ sub['brightness'] for sub in data ]
    else:
        time_of_day = data.get_time_of_day()
        brightness = data.get_brightness()
        daylight = data.get_daylight()
    
    # affichage des données
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    # Trace de la brightness en fonction du time_of_day
    ax1.plot(time_of_day, brightness, 'b.')
    ax1.set_xlabel('time_of_day')
    ax1.set_ylabel('brightness', color='b')
    for tl in ax1.get_yticklabels():
        tl.set_color('b')

    # Trace de la daylight en fonction du time_of_day
    ax2.plot(time_of_day, daylight, 'r.')
    ax2.set_ylabel('daylight', color='r')
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    
    if donnees_calculees:
        # affichage midi solaire   
        plt.axvline(x=midi_solaire, ymin=0, ymax=1, color="yellow",label = "midi solaire")
        
        # affichage lever-coucher
        plt.axvline(x=tod_lever, ymin=0, ymax=1, color="cyan",label = "lever du soleil (time of day)")
        plt.axvline(x=tod_coucher, ymin=0, ymax=1, color="orange",label = "coucher du soleil (time of day)")  
    elif analysable :
        plt.title("Données analysables", color="green")
    elif non_correlation :
        plt.title("Données Brightness et Daylight non correlées", color="orange")
    elif non_outdoor :
        plt.title("Données dont la plupart des photos sont indoor", color="red")

    plt.legend(loc="center right")
    plt.xlim(0, 1)
    plt.show()

def afficher_fichiers_dossier(doss):
    """
    Affiche tous les fichiers du dossier doss
    
    Args:
        doss(string): le chemin du dossier 
    
    Returns:
        void
    """
    for f in os.listdir(doss):
        data = photoset(join(doss,f))
        print("Fichier :", f, "Duree photoset (jours):", duree_photoset(data)*365, "Nombre outdoor < 0.5 :", sum([i<0.2 for i in data.get_outdoor()]))
        affichage_partition(data)
        
        
def nb_partitions_daylight_basse(data, est_partition=False):
    """
    Renvoie le nombre de partitions avec une daylight basse
    On a besoin de valeurs de daylight de nuit au moins deux fois dans la journée (sur deux intarvals distincts)
    La fonction retourne donc le nombre de periodes à bas daylight présentes sur le photoset (période = temps durant lequel le daylight ne quitte pas les basses valeurs (inferieures à eps=0.1 par defaut))
    
    
    Args:
        data(class photoset) : un objet de la class photoset
        partition(booleen) : true si l'on travaille avec une partition et non un photoset
    
    Returns: 
        entier : nombre de partitions avec une daylight basse
    """

    # avec daylight
    if est_partition:
        time_of_day = [ sub['time_of_day'] for sub in data ]
        daylight = [ sub['daylight'] for sub in data ]
    else:
        time_of_day = data.get_time_of_day()
        daylight = data.get_daylight()
        
    # valeur prises en dessous de 0.1 de daylight pour calculer debut et fin du jour
    epsilon = 0.1
    time_low_daylight=[]
    there_is_daylight_low = False
    k = 0
    for d in daylight:
        if d < epsilon:
            time_low_daylight.append(time_of_day[k])
            there_is_daylight_low = True
        k += 1

    densite_low_daylight = concentration(time_low_daylight)
    nb_periodes_low_daylight = 0
    periode_en_cours = False
    for part in densite_low_daylight:
        if len(part) > 0:
            if not periode_en_cours:
                nb_periodes_low_daylight += 1
                periode_en_cours = True
        else:
            if periode_en_cours:
                periode_en_cours = False

    #print("Nombre de periodes avec low daylight :", nb_periodes_low_daylight)
    #affichage_partition(data.usefuldata("photo"))
    return nb_periodes_low_daylight

def test_nb_jours_partition(fichs):
    """Fonction à copier coller dans le main pour exécution, permet de déterminer quel est le nombre optimal de jours
    de partition pour chaque interval de durée de fichier

    Args:
        fichs (list): lste de noms de fichiers .json (avec extension)
    """
    best_scores_nb_jours = []
    for f in fichs:
        print(f)
        data = photoset(f)
        best_score = 100000
        best_score_nb = -1
        a_ete_calcule = False
        for nb_jours_partition in range(1, 21):
            #print("Duree de prise de vue du photoset :", datetime_to_jours(duree_photoset(data)), "\nNombre de photos dans le set :", data.get_taille())
            duree_partition = nb_jours_partition
            
            #affichage_partition(data, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
            
            ### Calcul de la latitude
            # Partitione le jeu de donnee suivant une certaine periode (en jours)
            #duree_partition = 3 # en jours
            duree_max = 7
            nb_min_photo_set = 30
            partitions = photo_set_partiel_2point0(data, duree_partition, nb_min_photo_set, duree_max)

            # pour chaque partition, on realise le calcul de la latitude
            latitudes = []
            # caclucl du midi solaire et de la longitude pour chaque partition
            longitudes = []
            #print(len(partitions))
            for partition in partitions:
                #print(len(partition))
                # commence par le calcul de la duree du jour
                premier_jour = get_datetime_python(partition[0]["image_shooting"])
                # si la partition n'a pas de lever ET de coucher (nb de periodes low daylight < 2), alors on annule le calcul (généralement pour la derniere partition qui n'est pas sur un jour entier)
                if nb_partitions_daylight_basse(partition, partition=True) == 2 and not dans_periode_instable(premier_jour) :
                    tod_lever, tod_coucher = lever_coucher(partition)
                    #print(tod_lever, tod_coucher)

                    ### Latitude
                    # duree du jour
                    duree_jour = calculer_duree_du_jour(tod_lever, tod_coucher)
                    # jour de la premiere photo de la partition
                    lati = latitude(duree_jour, premier_jour)
                    latitudes.append(lati)
                    
                    ### Longitude
                    # calcul midi solaire (necessaire pour calcul longitude apres)
                    ms = midi(tod_lever, tod_coucher)
                    longi = longitude(ms, datetime_python_to_time_of_day(get_solar_noon_greenwich(premier_jour)))
                    longitudes.append(longi)
                    # possibilité ici d'afficher la partition avec lever du jour, coucher et midi_solaire
                    #affichage_partition(partition, tod_lever, tod_coucher, midi(tod_lever, tod_coucher))
                    #print("Premier jour du set :", premier_jour, "Lever :", tod_lever, "Coucher :", tod_coucher, "Midi-solaire :",ms, "Latitude :", lati, "Longitude :", longi)
                    #input()
                
            # ici peut-etre realiser la moyenne des latitudes de la liste latitudes
            if(len(latitudes)==0 or len(longitudes)==0):
                #print("toutes les partitions sont dans la période instable")
                print("", end="")
            else:
                a_ete_calcule = True
                lat = np.mean(latitudes)
                lon = np.mean(longitudes)
                lat2=data.data["photo_set"]["gps"]["latitude"]
                long2=data.data["photo_set"]["gps"]["longitude"]
                ecart_dist = geodesic((lat,lon), (lat2,long2)).km
                if ecart_dist < best_score:
                    best_score = ecart_dist
                    best_score_nb = duree_partition
        if a_ete_calcule:
            best_scores_nb_jours.append(best_score_nb)
    print(best_scores_nb_jours)
    print(np.mean(best_scores_nb_jours))

if __name__ == "__main__":
    def affichage(filename):
        data = photoset(filename)
        print("nom_fichier: ", data.nom_fichier)
        print("id: ", data.id)
        print("gps: ", data.gps)
        if data.gps:
            print("longitude: ", data.longitude)
            print("latitude: ", data.latitude)
        affichage_partition(data,donnees_calculees=True)
        print("Nombre de photos: ", data.get_taille(), "\n")
    filename = "fe64cf96.json"
    # affichage(filename)

    # # Data analysable
    # filename = "0b34ff9e"
    # data = photoset(filename)
    # affichage_partition(data, analysable=True)

    # # non correlation
    # filename = "1db50fea"
    # data = photoset(filename)
    # affichage_partition(data, non_correlation=True)

    # # non outdoor
    # filename = "0c577c14"
    # data = photoset(filename)
    # affichage_partition(data, non_outdoor=True)
    
    # a cheval sur 0-1
    filename = "0d38c5e9"
    data = photoset(filename)
    affichage_partition(data)
    
    


