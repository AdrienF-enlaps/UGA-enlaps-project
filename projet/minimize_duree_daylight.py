from astral import LocationInfo
from astral.sun import sun, azimuth, elevation, golden_hour, blue_hour, SunDirection
from scipy.optimize import minimize
from os.path import isdir,isfile,join
from class_photoset import photoset
from calcul_lever_coucher import photo_set_partiel_2point0, lever_coucher, calculer_duree_du_jour
from gestion_datetime import get_datetime_python, dans_jours_maudits
from analyse_donnees import duree_photoset, nb_partitions_daylight_basse, affichage_partition
import solar
import numpy as np
from datetime import datetime
import pandas as pd

def activation2(alpha, duree_jour_calc):
    return duree_jour_calc - alpha

def fun2(alpha, duree_jour_calc, duree_jour_reelle):
    a = alpha[0]
    y_hat = np.array([activation2(a, d) for d in duree_jour_calc])
    y = np.array(duree_jour_reelle)
    return np.mean(np.sum((y-y_hat)**2))

def trace_callback2(alpha):
    global i
    loss = fun2(alpha, duree_jour_calc, duree_jour_reelle) 
    d = {"alpha":alpha, "loss":loss, "iter": i}
    print(d)
    opt_traces.append(d)
    i += 1
    return False


# activation function
def activation(x, x0=0.03, scale=1):
    return 1. / (1 + np.exp(-scale*(x-x0)))

def elevation_from_loc(longitude, latitude, time):
    return np.array([elevation(LocationInfo(longitude=longitude, latitude=latitude).observer, dt) for dt in time])

# loss function
def fun(xZ, pos, y, sampled_utc):
    x_zero, sc = xZ
    longitude, latitude = pos
    y_hat = activation(elevation_from_loc(longitude, latitude, sampled_utc), x0=x_zero, scale=sc)
    # RMSE of activation function to observed data
    return np.mean(np.sum((y-y_hat)**2))

# callback to get evolution
def trace_callback(x):
    global i
    loss = fun(x, pos, activated_el, sampled_utc) 
    d = {"x0":x[0], "scale":x[1], "loss":loss, "iter": i}
    print(d)
    opt_traces.append(d)
    i += 1
    return False


class DictObject(object):
    def __init__(self, iterable=(), **kwargs):
       self.__dict__.update(iterable, **kwargs)

if __name__ == "__main__":
    
    # minimisation duree daylight
    doss = r"D:\Sauvegarde Sab\Cle_usb\Ecole\L3\Stage_applicatif\github\testgit\projet\Classement_gps\analysable"
    duree_partition = 3
    duree_max = 7
    nb_min_photo_set = 30
    duree_jour_calc = []
    duree_jour_reelle = []
    
    for f in  ['94fe5580.json', '6d6b2948.json', 'ea212b58.json', 'ae68d894.json', '35b6eb60.json', 'b3e1a3e9.json', 'c6b9f2fc.json', '2e302584.json', '941ea3db.json', '2984e0b2.json', '8d883fde.json', '065e1d53.json', '19ec674a.json', '7811e2fd.json']:
        data = photoset(join(doss,f))
        lat = data.latitude
        lon = data.longitude
        print("Latitude :", lat, "Longitude :", lon)
        partitions = photo_set_partiel_2point0(data, duree_partition, nb_min_photo_set, duree_max)
        for partition in partitions:
            premier_jour = get_datetime_python(partition[0]["image_shooting"])
            if nb_partitions_daylight_basse(partition, partition=True) == 2:
                #print(premier_jour)
                tod_lever, tod_coucher = lever_coucher(partition)
                duree_jour_calc.append(calculer_duree_du_jour(tod_lever, tod_coucher))
                duree_jour_reelle.append(solar.solar(lat, lon, premier_jour).get_sunlight())
                
        # affichage_partition(data)
        
    i = 0
    alpha0 = [0.95]
    opt_traces = []
    res = minimize(fun2, alpha0, args=(duree_jour_calc, duree_jour_reelle), method='BFGS', callback=trace_callback2)
                
# duree moins 1 semaine : alpha = 0.05469088 (partition 1 jour)          
# duree entre 1 sem et 1 mois : alpha = 0.05642733 (partition 3 jours)
# duree entre 1 et 3 mois : alpha = 0.06279758 (partition 3 jours)
# duree moins de 1 mois : alpha = 0.05647193 (partition 3 jours)
    '''
    
    # minimisation duree daylight sur algo enlaps
    doss = r"D:\Sauvegarde Sab\Cle_usb\Ecole\L3\Stage_applicatif\github\testgit\projet\Classement_gps\analysable"
    all_photosets = {}
    x0s = []
    scales = []
    for f in os.listdir(doss):
        file = join(doss, f)
        with open(file) as fh:
            data = json.load(fh)
            pid = data["photo_set"]["id"]
        all_photosets[pid] = data
        
    photosets_info = [{
    "id": k,
    "tz":  v["photo_set"]["tz"],
    "latitude": v["photo_set"]["gps"]["latitude"],
    "longitude": v["photo_set"]["gps"]["longitude"],
    "altitude": v["photo_set"]["gps"]["altitude"],
    }
    for k,v in all_photosets.items()]
    # photosetd_df = pd.DataFrame.from_dict(photosets_info, orient="index")
    print(photosets_info)
    
    for test_idx in range(0, len(photosets_info)):
        photoset_id = photosets_info[test_idx]["id"]

        print(photosets_info[test_idx])

        # test data
        df = pd.DataFrame.from_records(
            [
                { 
                    "photoset_id": pid,
                    "image_shooting": datetime.fromisoformat(img["image_shooting"]+"+00:00"),
                    "daylight": img["attributes"]["daylight"],
                    "brightness": img["brightness"]/128,
                }  
                for pid,pinfo in all_photosets.items() for img in pinfo["photo_set"]["photos"]
            ])
        filtered_df = df[df.photoset_id == photoset_id]

        # optim raw data
        sampled_utc = filtered_df["image_shooting"]
        activated_el = filtered_df["daylight"].values

        # groundtruth
        loc = DictObject(photosets_info[test_idx])

        # position connue
        pos = (loc.longitude, loc.latitude)
        # start parameters x0, scale
        xZ = [0, 1]
        # minimization
        opt_traces = []
        i = 0
        res = minimize(fun, xZ, args=(pos, activated_el, sampled_utc), method='BFGS')
        x0s.append(res['x'][0])
        scales.append(res['x'][1])
        print(max(sampled_utc) - min(sampled_utc))
        print(res['x'])
        
    print("Moyenne des x0 :", np.mean(x0s), "Moyenne des scale :", np.mean(scales))
    # resultat : Moyenne des x0 : -3.5662702494409184 Moyenne des scale : 0.6089179671520482
'''  

