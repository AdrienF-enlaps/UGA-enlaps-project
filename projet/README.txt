
Utilisation :
    Le programme utilisable est à retrouver dans le notebook Notebook_Main.ipynb, les instructions sont données pour l'exécution des différentes parties du programme

Annexe:

datetime_json : format de datetime rencontré dans les fichiers .json fournis par ENLAPS. 
Réel désignant la date et l'heure au format d'une année.
C'est à dire que 0 représente le premier jour de l'année à 00:00:00  et 1 le dernier jour de l'année à 23:59:59
Exemple:
    2021-11-14 06:45:01 correspond à 2021.86926373034
    2019-10-08T18:15:04 correspond à 2019.7692067478438


time_of_day : format de l'heure rencontré dans les fichiers .json fournis par ENLAPS.
Réel compris entre 0 et 1 représentant une heure normalisée
C'est à dire que 0 représente 00:00:00 et 1 donne 23:59:59
Exemple:
    06:40:03 correspond à 0.2778125
    21:07:17 correspond à 0.8800578703703704


datetime python : objet du module datetime de python (voir documentation en ligne)

daylight : valeur de confiance calculé par une intelligence artificielle. 
    C'est un flottant compris entre 0 et 1 représentant la certitude d'être en journée.
    Par exemple 0.1 signifie qu'il fait surement nuit et au contraire 0.99 indique qu'il fait 
    très probablement jour.

brightness : flottant compris entre 0 et 255 donnant la luminositée de l'image.





