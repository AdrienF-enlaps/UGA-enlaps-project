#import daylight, pytz
import json
from os.path import join,isfile,abspath,dirname
from datetime import datetime
from datetime import timedelta

class photoset:
    # Initialisation du fichier .json
    def __init__(self, filename):
        """Construire la structure de donnée

        Args:
            filename (string): Ouvre le fichier correspondant
        """
        chemin_code = abspath(__file__)
        chemin_projet = dirname(chemin_code)
        chemin_gps = join(chemin_projet, "gps")
        chemin_no_gps = join(chemin_projet,"no_gps")

        self.nom_fichier = filename.split('\\')[-1].split('/')[-1].split(".")[0] + ".json"
 
        if isfile(join(chemin_no_gps,self.nom_fichier)):
            self.gps = False
            f = open(join(chemin_no_gps,self.nom_fichier),'r')

        elif isfile(join(chemin_gps,self.nom_fichier)):
            self.gps = True
            f = open(join(chemin_gps,self.nom_fichier),'r')
        

        self.data = json.load(f)

        self.id = self.data["photo_set"]["id"]
        self.photos = self.data["photo_set"]["photos"]

        if self.gps == True:

            self.longitude = self.data["photo_set"]["gps"]["longitude"]
            self.latitude = self.data["photo_set"]["gps"]["latitude"]
            self.altitude = self.data["photo_set"]["gps"]["altitude"]

        f.close()

    # Methode gestion des photo
    # Liste ou element de la liste de photo
    def get_photo(self, indice=None):
        """
        Retourne la ieme photo du set si i different de None
        Sinon retourne la liste des photos du photo_set
        
        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le set de la photo retournée 
        Returns:
            photo: la photo à la position indice
        """
        return self.photos[indice] if indice != None else self.photos

    def get_taille(self):
        """
        Retourne la taille du photo_set courant
        
        Args:
            self (photo_set): le photo_set courant
        Returns:
            entier: la taille du photo_set courant
        """
        return len(self.photos)
    # outdoor

    def get_outdoor(self):
        """
        Retourne la liste des outdoor des photos du photo_set courant
        
        Args:
            self (photo_set): le photo_set courant
        Returns:
            liste de photos
        """

        outdoor = []
        for photo in self.photos:
            outdoor.append(photo["attributes"]["outdoor"])
        return outdoor

    # id
    def get_id(self, indice=None):
        """
        Renvoie l'id de la ième photo si indice != None
        Sinon retourne la liste des id du photo_set courant

        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée 
        Returns:
            un entier: l'id de la photo 
            
            ou
            
            une liste d'entier: liste des id des photos du photo_set
        """
        if (indice is not None):
            return self.get_photo(indice)["id"]

        photos = self.get_photo()
        id = []
        for photo in photos:
            id.append(photo["id"])
        return id
    # image filename

    def get_image_filename(self, indice=None):
        """
        Renvoie le image_filename de la ième photo si i != None
        Sinon retourne la liste des image_filename du photo_set courant

        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            string: image_filename
            
            ou 
            
            liste de string: liste d'image_filename
        """
        if (indice is not None):
            return self.get_photo(indice)["image_filename"]

        image_filename = []
        for photo in self.photos:
            image_filename.append(photo["image_filename"])
        return image_filename
    # image shooting

    def get_image_shooting(self, indice=None):
        """
        Renvoie le image_shooting de la ième photo si i != None
        Sinon retourne la liste des image_shooting du photo_set courant

        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            string: image_shooting
            
            ou 
            
            liste de string: liste d'image_shooting
        """
        if (indice is not None):
            return self.get_photo(indice)["image_shooting"]

        image_shooting = []
        for photo in self.photos:
            image_shooting.append(photo["image_shooting"])
        return image_shooting
    # datetime

    def get_datetime(self, indice=None):
        """
        Renvoie le datetime de la ième photo si i != None
        Sinon retourne la liste des datetimes du photo_set courant

        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            float: datetime
            
            ou 
            
            liste de float: liste de datetime
        """
        if (indice is not None):
            return self.get_photo(indice)["datetime"]

        datetime = []
        for photo in self.photos:
            datetime.append(photo["datetime"])
        return datetime
    # time_of_day

    def get_time_of_day(self, indice=None):
        """
        Renvoie le time_of_day de la ième photo  si i != None
        Sinon retourne la liste des time_of_day du photo_set courant
        
        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            float: time_of_day
            
            ou 
            
            liste de float: liste de time_of_day
        
        """
        if (indice is not None):
            return self.get_photo(indice)["time_of_day"]

        time_of_day = []
        for photo in self.photos:
            time_of_day.append(photo["time_of_day"])
        return time_of_day
    # brightness

    def get_brightness(self, indice=None):
        """
        Renvoie le brightness de la ième photo si i != None
        Sinon retourne la liste des brightness du photo_set courant

        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            float: brightness
            
            ou 
            
            liste de float: liste de brightness
        
        """
        if (indice is not None):
            return self.get_photo(indice)["brightness"]

        brightness = []
        for photo in self.photos:
            brightness.append(photo["brightness"])
        return brightness
    # daylight

    def get_daylight(self, indice=None):
        """
        Renvois le daylight de la ième photo si i != None
        Sinon retourne la liste des daylight du photo_set courant
        
        Args:
            self (photo_set): le photo_set courant
            
            indice (entier): la position dans le photo_set de la photo retournée
        Returns:
            float: daylight
            
            ou 
            
            liste de float: liste de daylight
        
        """
        if (indice is not None):
            return self.get_photo(indice)["attributes"]["daylight"]

        daylight = []
        for photo in self.photos:
            daylight.append(photo["attributes"]["daylight"])
        return daylight

    # All
    def get_all(self,  *args, **kwargs):
        """
        Liste des données:
            "image_filename"
            "image_shooting"
            "datetime"
            "daylight"
            "brightness"
            "time_of_day"
        Renvoie une liste d'information passées en argument des photos 
        dont l'argument kwargs est une liste d'id de photo traité
        
        Si l'argument kwargs est une id renvoie les informations de
        la fonction correspondante 

        Sinon
        Renvoie une liste des informations passées en argument de chaque 
        photos du photoset    
        
        Args:
        
            args(str): argument de la fonction représentant les données souhaitée  
                
            kwargs(liste de str ou entier):
                Concerne uniquemant la donnée "id"
                Si passé en argument (.."id" = liste des id .. )
                    Renvoie les photos correspondantes
                Si passe en argument (.."id" = entier ..)
                    Renvoie la photo correspondante
                
        Sinon
            Renvoie un dictionnaire contenant:
                -- Id du photo_set
                -- Liste des données entrée en parametres
        
        Returns:
            Dict :Dictionnaire avec pour chaque id photo les informations souhaitées

        """
        # Toute les donnée
        if 'all' in args or 'All' in args or len(args) == 0:
            return self.usefuldata()
        else:
            # initialiser le dictionnaire
            Dico_Usefuldata = {
                "photo_set": {
                    "id": self.data["photo_set"]["id"]
                }
            }
            d = {}
            if "id" not in kwargs:
                # Liste des info pour chaque photo
                Usefuldata_list = []
                i = 0
                # Definir l'ensemble des donnée demandées
                for photo in self.data["photo_set"]["photos"]:

                    d["id"] = self.get_id(i)
                    if "image_filename" in args:
                        d["image_filename"] = self.get_image_filename(i)
                    if "image_shooting" in args:
                        d["image_shooting"] = self.get_image_shooting(i)
                    if "time_of_day" in args:
                        d["time_of_day"] = self.get_time_of_day(i)
                    if "datetime" in args:
                        d["datetime"] = self.get_datetime(i)
                    if "brightness" in args:
                        d["brightness"] = self.get_brightness(i)
                    if "daylight" in args:
                        d["daylight"] = self.get_daylight(i)
                    Usefuldata_list.append(dict(d))
                    i += 1

                # Ajouter la liste au dictionnaire

                Dico_Usefuldata["photos"] = Usefuldata_list
                # Renvoyer les donnée demandée
                return Dico_Usefuldata
            else:
                # Si l'id est passée en argument
                # sous forme d'entier
                    if type(kwargs["id"]) == int:
                        for photo in self.get_photo():
                            if photo["id"] == kwargs["id"]:
                                d["id"] = photo["id"]
                                d["brightness"] = photo["brightness"]
                                d["datetime"] = photo["datetime"]
                                d["daylight"] = photo["attributes"]["daylight"]
                                d["image_filename"] = photo["image_filename"]
                                d["image_shooting"] = photo["image_shooting"]
                                d["time_of_day"] = photo["time_of_day"]
                                Dico_Usefuldata["photos"] = [dict(d)]
                        return Dico_Usefuldata
                # Sous forme de liste
                    elif type(kwargs["id"]) == list:
                        Usefuldata_list = []

                        for photo in self.get_photo():
                            if photo["id"] in kwargs["id"]:
                                d["id"] = photo["id"]
                                if "image_filename" in args:
                                    d["image_filename"] = photo["image_filename"]
                                if "image_shooting" in args:
                                    d["image_shooting"] = photo["image_shooting"]
                                if "time_of_day" in args:
                                    d["time_of_day"] = photo["time_of_day"]
                                if "datetime" in args:
                                    d["datetime"] = photo["datetime"]
                                if "brightness" in args:
                                    d["brightness"] = photo["brightness"]
                                if "daylight" in args:
                                    d["daylight"] = photo["attributes"]["daylight"]
                                Usefuldata_list.append(dict(d))
                        Dico_Usefuldata["photos"] = Usefuldata_list
                        return Dico_Usefuldata
                    else:
                        print("[Error type ] kwargs argument")

#    def get_solar_noon(self):
#        sun = daylight.Sunclock(51.477928, -0.001545)
#        t = sun.solar_noon(image_filename.get_datetime().timestamp())
#        print(datetime.utcfromtimestamp(t))

# Donnée utile

    def usefuldata(self,*args):
        """
        Retourne une liste d'information utile des photo du photoset
            "id"
            "image_filename"
            "image_shooting"
            "datetime"
            "daylight"
            "brightness"
            "time_of_day"
        Returns:
            Dico_Usefuldata(dict): Dictionnaire correspondant au information 
                                    souhaitée       
        """
        
        if "photos" not in args:
            Dico_Usefuldata = {
                "photo_set": {
                    "photos": [],
                    "id": self.id
                }
            }
    # Liste de photo des photos du photo_set
        Usefuldata_list = []

        for photo in self.photos:

            Usefuldata_list += [{"id": photo["id"],
                                 "image_filename":photo["image_filename"],
                                 "image_shooting":photo["image_shooting"],
                                 "datetime":photo["datetime"],
                                 "time_of_day":photo["time_of_day"],
                                 "brightness":photo["brightness"],
                                 "daylight":photo["attributes"]["daylight"]
                                 }]
            # Si photos du photo_set demander demandee
        if "photos" not in args:    
            Dico_Usefuldata["photo_set"]["photos"] = Usefuldata_list
        
        if "photos" not in args:
            return Dico_Usefuldata
        else :
            return Usefuldata_list

if __name__  == "__main__":
    def affichage(filename):
        data = photoset(filename)
        print("nom_fichier: ",data.nom_fichier)
        print("id: ", data.id)
        print("gps: ", data.gps)
        if data.gps:
            print("longitude: ",data.longitude)
            print("latitude: ",data.latitude)
        
        print("Nombre de photos: ",data.get_taille(),"\n")
    filename = "fe64cf96.json"
    affichage(filename)