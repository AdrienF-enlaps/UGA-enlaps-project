# GPS from metadata

The goal of this project is to study to which extent we can infer the position of a timelapse from its image metadata.

An example of metadata for a given project:

```json
{
    "photo_set": {
        "id": 34845,
        "photos": [
            {
                "image_filename": "8123a6f0-2f32-4758-b5f8-99e4930a50d6.jpg",
                "image_shooting": "2020-08-12T06:15:05",
                "id": 49500613,
                "time_of_day": 0.26047453703703705,
                "datetime": 2020.6127335369865,
                "micro_thumb": [
                    [
                        [
                            202.129638671875,
                            213.1065216064453,
                            230.95362854003906
                        ],
                        [
                            104.41313934326172,
                            128.46014404296875,
                            166.91444396972656
                        ],
                        [
                            62.22886276245117,
                            87.52792358398438,
                            137.5023193359375
                        ],
                        [
                            62.406455993652344,
                            89.87935638427734,
                            142.5739288330078
                        ]
                    ],
                    [
                        [
                            77.95538330078125,
                            72.14360809326172,
                            65.0481948852539
                        ],
                        [
                            53.19447708129883,
                            49.00680160522461,
                            40.353492736816406
                        ],
                        [
                            61.49129867553711,
                            52.086978912353516,
                            37.65751647949219
                        ],
                        [
                            76.96907806396484,
                            67.99654388427734,
                            41.09278106689453
                        ]
                    ]
                ],
                "brightness": 109.91191276550293,
                "attributes": {
                    "outdoor": 1.0,
                    "daylight": 0.9854210615158081
                }
            },
            ...
        ]
    }
}
```

2000 images per photoset were sampled and given to students.

Description of the fields:
- `time_of_day` : normalized time of day. `0.` means `00:00:00`, `1.0` means `23:59:59`
- `datetime`: floating point representation of time. The integer part is the year, the decimal part is `(n_seconds_from 01-01 00:00:00) / (total seconds in year)`
- `micro_thumb` : an 4x2 RGB miniature image
- `brightness` : brightness of the image, in [0,255], computed on micro_thumb
- `outdoor` : AI classification result. Confidence in the image being shot in an outdoor environment.
- `daylight` : AI classification result. Confidence in the image being shot during day.